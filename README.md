PROJECT MIRAGE

Le but de ce projet est de monter, en one shot, toute une infra contenant les elements suivants :
* pihole
    - cloudflared
* vaultwarden
* forgejo (gitea)
* grafana
    - loki
    - prometheus
* speedtest
* homeassistant
* nginxProxyManager

Tous ces elements sont montés avec ansible et podman

