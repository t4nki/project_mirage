clear
banner_width=43
colonnes=$(tput cols)
indent=$(( (colonnes - banner_width) / 2 ))
prefix=''
for ((i=1; i<=indent; i++)) ; do
    prefix+=' '
done

echo "${prefix}             [38;5;111m[1m.:--==+++++==--:."
echo "${prefix}          :-+++++=-------=+++++=:"
echo "${prefix}       .=+++=:.             .:=+++=:"
echo "${prefix}     .=++=:                     :=++=:"
echo "${prefix}    -++=:                         .=++="
echo "${prefix}  .=++-                             -++=."
echo "${prefix} .=++:                    [0m.:-.       [38;5;111m[1m:+++."
echo "${prefix} =++:         [0m:-::.     :=+++.        [38;5;111m[1m:++="
echo "${prefix}:++=          [0m.-++++=--=+++-           [38;5;111m[1m=++:"
echo "${prefix}-++:             [0m:=++++++-             [38;5;111m[1m:++-"
echo "${prefix}=++:              [0m:++++++=             [38;5;111m[1m.++="
echo "${prefix}=++:         [0m::::=+++-=+++:            [38;5;111m[1m:++="
echo "${prefix}:++=         [0m.:=+++-   :+++            [38;5;111m[1m-++-"
echo "${prefix} +++.           [0m:++.    .=+-          [38;5;111m[1m.+++"
echo "${prefix} :++=             [0m-.      .           [38;5;111m[1m=++:"
echo "${prefix}  :++=.                             .=++-"
echo "${prefix}   :+++:                           :+++:"
echo "${prefix}     -++=:                       :=++=."
echo "${prefix}      .-+++-:                 :-+++=."
echo "${prefix}         :=++++=-:.......:--=+++=:"
echo "${prefix}            .-==+++++++++++==-.[0m"
echo "${prefix}                M I R A G E"

export PS1='\[\e[0;38;5;32m\][\[\e[0;37m\]\A \[\e[0;38;5;32m\]| \[\e[0;37m\]\u\[\e[0;38;5;32m\]@\[\e[0;37m\]\h\[\e[0;38;5;32m\]] \[\e[0;37m\]\w \[\e[0;38;5;32m\]\$ \[\e[0m\]'
export HISTCONTROL=ignoreboth:erasedups
export SYSTEMD_EDITOR=vim
