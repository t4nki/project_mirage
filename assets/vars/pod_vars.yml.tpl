---
#nom des pods
pod_names: infra hassio metrics

#IP du pod infra
infra_inet:
#ports à mapper pour le pod infra
infra_ports:
  - '{{ ansible_default_ipv4.address }}:53:53/tcp'
  - '{{ ansible_default_ipv4.address }}:53:53/udp'
  - '80:80'
  - '127.0.0.1:81:81'
  - '443:443'
  - '2222:22'
  - '3128:3128'

#IP pod home assistant
hassio_inet:
#port à mapper pour home assistant
hassio_ports:
  - '1883:1883'

#ip pod métriques
metrics_inet:
#port à mapper pour le pod de métriques
metrics_ports:
  - '3100:3100'

gitVer: '1.20'
