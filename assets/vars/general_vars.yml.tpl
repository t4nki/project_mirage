---
#configuration wrt true|false
enable_wrt:
#configuration tasmota true|false
enable_mqtt:
#fqdn du routeur
wrt_host:
#protocole du routeur http|https
wrt_proto:
#user pour npm
npm_login:
#user pour grafana
grafana_login:
#user pour grafana-to-ntfy
g2n_login:
#coordonées pour openspeedtest
speedtest_lat: 44.863271
speedtest_lon: -0.621004
#nom du réseau podman
net_name: '{{ ansible_hostname }}_net'
#notation CIDR pour la création du réseau podman O.O.O.O/0
net_subnet:
#hotes à créer dans NPM
redir_hosts:
#service à sauvegarder
service_data:
#hote pour la redirection
forward_hosts:
#sous domaine pour la redirection spice pour proxmox
stream_hosts:
#hotes tasmota pour la configuration mqtt
tasmota_hosts:
#définition de l'architecture cpu pour les binaires à télécharger
arch: "{% if ansible_architecture == 'aarch64' %}arm64{% else %}{{ ansible_architecture }}{% endif %}"
#repertoire à copier via rsync contenant les sauvegardes
backup_folder:
