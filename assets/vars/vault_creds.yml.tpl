---
#Fichier à éditer avec ansible-vault
#mot de passe pour le routeur
vault_wrt_passwd:
#mot de passe pour pihole
vault_pihole_passwd:
#mot de passe pour NPM
vault_npm_passwd:
#mot de passe pour grafana
vault_grafana_passwd:
#mot de passe pour tasmota
vault_tasmota_passwd: 
#token longue durée home assistant
vault_hassio_token:
