#!/bin/bash

apt update
apt upgrade -y
apt install -y --no-install-recommends --no-install-suggests vim htop lm-sensors curl bridge-utils git ansible cockpit cockpit-machines cockpit-podman cockpit-storaged cockpit-networkmanager netavark aardvark-dns qemu-system qemu-block-extra qemu-utils libvirt-clients-qemu virtinst qemu-efi-aarch64 catatonit python3-jmespath jq ufw unzip python3-passlib rsync gdu wakeonlan
rm /etc/localtime 
ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
sed -i 's/.*PermitRootLogin.*/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config
passwd
mkdir /root/.ssh
chmod 700 /root/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCaXVliUupf1sUwBVofB61yX2dTQCLRnar851q17tZTeLrOTTsK1acptJo7gvapL0wSIYdbvPsbsGjJaVieAW5qPI8L0C0r9x+rniSQaKoN/m2hzMYjflLMy4xu9dpztna6hXccPT6Sy4eWDjssJBe5lOHtOCofHG0/pFr0mSf1fJyVu1QtvJSmbTes3wH0GwMZH0lRMWkgiHnQ28OVvRjasJElvfUXaDFkooPLX7kiR8ulgTVLuLy+JpYvhBU9ZAADXKgzSQo9p255JlsKz05jtA/H1fElkiZlFLcku1TLTE7g/e4LS0GHUa/B1sFCWQKaj1ddhhSR0fi2VgM0oXHJ tanki@Mozafoka" > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys
systemctl enable ssh.service
cat <<IP6 > /etc/sysctl.d/99-disable_ipv6.conf
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
IP6
cat <<FBM > /etc/modprobe.d/raspi-blacklist.conf
blacklist async_memcpy
blacklist async_pq
blacklist async_raid6_recov
blacklist async_tx
blacklist async_xor
blacklist backlight
blacklist bluetooth
blacklist brcmfmac
blacklist brcmutil
blacklist btbcm
blacklist cec
blacklist drm
blacklist drm_display_helper
blacklist drm_panel_orientation_quirks
blacklist hci_uart
blacklist linear
blacklist md_mod
blacklist pisp_be
blacklist raid0
blacklist raid1
blacklist raid10
blacklist raid456
blacklist raid6_pq
blacklist rpivid_hevc
blacklist snd
blacklist snd_bcm2835
blacklist snd_pcm
blacklist snd_pcm_dmaengine
blacklist snd_soc_core
blacklist snd_soc_hdmi_codec
blacklist v3d
blacklist vc4
blacklist videobuf2_common
blacklist videobuf2_dma_contig
blacklist videobuf2_memops
blacklist videobuf2_v4l2
FBM
nmcli connection add ifname br0 type bridge con-name br0
nmcli connection add type bridge-slave ifname eth0 master br0 
nmcli connection modify br0 bridge.stp no
nmcli connection up br0
read -p "Indiquez le hostname : " hstnm
hostnamectl set-hostname ${hstnm}
virsh net-undefine --network default
cat <<HBF > /tmp/host-bridge.xml
<network>
  <name>host-bridge</name>
  <forward mode="bridge"/>
  <bridge name="br0"/>
</network>
HBF
virsh net-define /tmp/host-bridge.xml
virsh net-start host-bridge
virsh net-autostart host-bridge
ln -s /usr/share/qemu/firmware/60-edk2-aarch64.json /usr/share/qemu/firmware/30-edk2-aarch64.json
cat <<FCA > /etc/profile.d/alias.sh
alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -la'
alias lr='ls -laR'
alias vi='vim'
alias padd='podman exec -ti pihole padd'
alias poweroff='echo "Extinction impossible"'
FCA
touch /{root,etc/skel}/.hushlogin
echo -ne "" > /etc/motd
sed -i 's/root/#root/' /etc/cockpit/disallowed-users
systemctl enable cockpit.socket
cat <<FCC > /etc/cockpit/cockpit.conf
[WebService]
LoginTitle=${hstnm}
LoginTo=false
FCC
systemctl disable wpa_supplicant apparmor.service apt-daily.service apt-daily.timer apt-daily-upgrade.service apt-daily-upgrade.timer podman-auto-update.timer podman-auto-update.service bluetooth.service hciuart.service dphys-swapfile.service rpi-display-backlight.service sshswitch.service triggerhappy.socket triggerhappy.service
sed -i 's/console.*tty1/console=tty1 cgroup_enable=memory cgroup_memory=1 ipv6.disable=1/g' /boot/firmware/cmdline.txt
update-initramfs -u -k all
systemctl daemon-reload
apt clean
dpkg-reconfigure locales
sleep 2
reboot

